# Lenovo Thinkpad T440p Upgrade Guide

## About the device

The Thinkpad T440p can be seen by the enthusiast community as the one of the last few great laptops in the past decade. What makes the T440p so special over modern day laptops is the ability to upgrade almost every component, from the CPU (yes, like on a desktop) to the keyboard. It was a great price/performance option back in 2018 when it could offer ~90% CPU performance of a then-current i7 T480 at less than a third of the cost.

Today, it is undeniable that the T440p is aging but it still holds up better as a workhorse than it's x40 generation stablemates which introduced underperforming ULV dual cores (T440/X240), gimped RAM capacities (T440/X240), an awful keyboard (X1 Carbon Gen 2), or minor performance improvements at the expense of battery life, weight, and cost (W54x). This is largely due to the fact that the weaknesses of the T440p could be mitigated by upgrades which this guide will address in great detail.

### Why should I buy the T440p (over similarly priced laptop today)?

- Great **build** quality
- Easy **upgrade** options
- Acceptable **performance** (even the 4300M is better than a crappy ULV dual core)
- Serves as a great **Linux** laptop (great for Computer Science students)
- Great selection of **ports** (even more with a _ultra-dock_)

### Why shouldn't I buy the T440p?

- Renoir-based Ryzen ThinkPads such as the base 4500U E14 are more efficient and greatly outperform a decked out T440p at a similar cost
- Parts availability isn't what it used to be and costs are higher than when this guide was first written (eg: 4700MQs used to be $50 but are closer to $100 now), if they're not already extinct like the dGPU heatsink
- The time to fully setup a T440p depends on the shipping of about half a dozen parts which will take a while these days
- The **touchpad** that comes out of the factory can be considered as _garbage_
- Some parts don't feel as sturdy as you'd want it to be (e.g. the screen has some degree of flex)
- Cost of upgrades likely outweigh the benefits of buying a laptop new or second-hand
- Bulky and heavy by contemporary standards (consider carefully if you want to buy this for school)
- Limited Wifi card upgrade options[^1]
- Keyboard layout will take some time to get used to (e.g. Fn and ctrl keys)[^2]

[^1]: Wifi card upgrade problem can be solved using a custom bios mod

[^2]: Fn/Ctrl key layout can at least be swapped in bios

### Where to find?

You can find used thinkpads all over the internet. This is due to the fact that Thinkpads are targeted towards enterprise environments. Organisations purchase these laptops at large quantities, and once deemed unfit, they may make them available in the used market.

When searching for any used laptop, use common sense. Look at seller reputation (only to a certain degree) and the advertised quality grade. You can generally get away with a _B-grade_ laptop, however **A-grade or higher** is even better. When looking at the specs, _avoid_ getting the model with the Nvidia dedicated graphics. It is known to be _under-powered_ and will be a _battery hog_.

If you are on a tight budget or simply want a bargain, consider looking at **auctions** (e.g. eBay). There is a higher chance that you can pay a lot less than what people are offering outright.

### How much to pay?

I (the author) live in Australia and so the used market can be a bit of a mixed-bag. You can get great deals, but that's usually rare.

I paid around $260 (AUD) for my T440p which had the Core i5 4300m, 4GB of RAM and a 500GB Mechanical Drive. There were other T440p models available too, but I opted only for the base model. You could probably do a lot better than I did, however I found the seller to be very helpful after sending them a few queries with regards to the device.

## Processor (CPU)

<div class="video-responsive">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/YetQcY8gq10" frameborder="0" allowfullscreen></iframe>
</div>

The T440p can be found equipped with a processor ranging from the dual-core Intel Core i3-4000M all the way to a quad-core 8-thread Intel Core i7-4900MQ. The most common processor that can be found in the T440p is the Core i5-4300M.

Whilst the i5-4300M is perfectly fine for everyday tasks such as watching a video, browsing the web or creating documents, some tasks such as development and virtual machines may struggle on the dual-core.

### Recommended processors

|Processor     |TDP|Comments
|--------------|---|--------
|Core i7 4702MQ|37W|Recommended
|Core i7 4712MQ|37W|Also recommended if same price or cheaper. Slightly faster.
|Core i7 4700MQ|47W|The extra 10W TDP and higher boost clock means it'll run hotter
|Core i7 4800MQ|47W|Like the 4700MQ but slightly faster but hotter

There really isn't any point on getting a processor that is higher specced since the cooling capacity of the T440p will severely limit how often it can obtain its max clocks.

I personally have the 4700mq and can notice how much hotter the system runs when doing the same tasks as the Core i5-4200M that was previously on my system.

I won't provide a guide on how to install your new CPU since there are already guides that show this, and since the process is fairly straightforward anyway.

### Tips to achieve better battery life and longevity

<div class="video-responsive">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/vfIxf73RGEg" frameborder="0" allowfullscreen></iframe>
</div>

So now you have installed your new CPU and you notice that your CPU is running a little hot and your battery isn't lasting that long. Well there are ways you can solve this, but they won't be simple to do as there are many factors that can affect how well these procedures go.

#### Windows

1. Disable turbo boost on battery
    - Turbo boost whilst great for performance, can quickly drain your battery especially when boosting over long periods of time
    - Go to battery options -> profiles -> edit profile -> processor management -> maximum processor state and modify the value for battery to be 99% or under
2. Use thottlestop to undervolt your CPU
    - Undervolting your CPU can greatly reduce thermals while also increasing performance in some cases (power throttling scenarios)
    - Download throttlestop, extract the archive and open the application
    - Click on the FIVR button and you should see the options to undervolt different components of your CPU
    - Enable voltage adjustments and apply a moderate undervolt on the CPU core and cache. Start with -50mv and work your way until your system gets unstable

You can test the stability of the system using Prime95 or Cinebench.

**Note: When using prime 95, ensure that you untick the AVX checkboxes. It will only generate an excess amount of heat that your cooling can't handle (hence easily reaching the maximum allow temperates)**

#### Linux

1. Install TLP and disable turbo boost
    - TLP is one of the best powersaving utilities avaiable on Linux operating systems. It perform many functions of similar tools (e.g powertop), without having the user worry about perfoming them manually
    - Install TLP using your Linux distibution's package manager
        - For ubuntu: 

                sudo apt install tlp tlp-rdw

    - The default configuration should be fine for most people, however we will be modifying it for better battery life
    - Open `/etc/default/tlp` with your favourite text editor (e.g. nano, vim)
    - Uncomment these lines and save the changes:

            CPU_BOOST_ON_AC=1
            CPU_BOOST_ON_BAT=0

    - Apply these changes by running:

            sudo tlp start

    - Depending on the distribution you are using, if you run sudo tlp-stat -s, the application might ask you to enable/mask some services, run the commands as it has instructed you to
2. Install undervolt
    - Undervolting in Linux is fairly easy, however we won't be using any graphical interface this time
    - Visit: <https://github.com/georgewhewell/undervolt>
    - If you are unable to run any of the undervolt commands, try running as root
    - There is a way to get your throttlestop configs which is cool (no pun intended) if you want to keep things consistent
    - If you run the systemd service and it fails, it is probably pointing to incorrect paths. Try running ```whereis undervolt``` to get the correct path and change the service unit config if required.

#### Undervolting Expectations

The Haswell series of Intel chips can be undervolted moderately, but not as great as Skylake and newer. It also largely depends on silicon lottery. Here are the voltage offset ranges in which each component could reasonably be expected to be undervolted:

|Core         |Cache        |GPU
|-------------|-------------|-----------
|-50 to -100mV|-50 to -100mV|-30 to 50mV

In my case, my chip handles undervolting quite well at -100mV on the core and cache and -50mV on the GPU but your experience may be different so it is best to experiment. I could go undervolt more but I haven't had any issues as of yet, so I haven't bothered touching these settings.

Unstable undervolt settings can seem stable at first followed by crashing that often occurs when the system is running at a low clockspeed or under light load which make stress testing regimes like those used for overclocking irrelevant. There isn't really a good way to determine the stability of an undervolt besides general usage until the system crashes out of the blue so a good rule of thumb is to find the voltage setting that causes instant crashing and reducing that offset by 40mV. For example, if my hypothetical 4810MQ crashes instantly at -90mV on the core but not -80mV, that I'd set a -50mV undervolt and call it a day.

## Memory (RAM)

<div class="video-responsive">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/F2PWKpVj4JE" frameborder="0" allowfullscreen></iframe>
</div>

The memory modules on the T440p are **not** soldered (thankfully). Removable RAM is becoming less of a commonplace for newer laptops as they focus more on efficiency and portability. The T440p supports up to 16GB of DDR3L RAM, with a max speed of 2133MHz.

One thing that I've found through my own experience was that the **T440p can support RAM modules of different voltages**. I currently have one module running at 1.35V and the other running at 1.5V. I don't see a massive problem with regards to battery.

### What to buy?

As with building a desktop, **it is best to use some RAM from reliable manufacturers**. You may find a good deal on your local eBay, or maybe have some modules lying around somewhere.

For the best value and to save yourself trouble, find yourself some **8GB 1600MHz sticks** as the performance benefit going from 1600MHz to 2133MHz should be neglible.

### BIOS updates and performance

Lenovo has provided a bios update to help mitigate the effects of the **rowhammer vulnerability which incurs a ~5% performance penalty**. Whether or not it fixes this vulnerablity, I would still update your bios **regardless as they usually include other security updates**.

## Storage

In the past, solid state media, or morely commonly 'SSDs', lacked the necessary storage size and cost effectiveness that traditional spinning disks could offer. Nowadays, SSDs have become much more *affordable* and are now **play a crucial role to how well a system can perform.**

### Upgrade options

The T440p provides several options for storage:

1. A traditional 2.5 inch drive bay
2. A m.2 slot (which may already be populated with a WWAN card)
3. An additional 2.5 inch drive using a Ultrabay caddy

#### Traditional 2.5 inch drive bay

The easiest option to go for is to replace the drive that is already in the 2.5 inch drive bay. What you'll usually find in the T440p is a stock standard 7200RPM spinning hard disk whilst decent, it is still very much slower as compared to a SSD.

The bay can fit any standard SSD available in the market, but this doesn't mean that you should just buy the cheapest available SSD. It is recommended that you go with a reliable brand like Samsung or Crucial (a simple Google search should bring up some reliable SSD manufacturers).

#### m.2 slot

The m.2 slot requires a **2242-keyed** SATA SSD, which severely limits the amount of options you can choose from. This layout is uncommon, where most manufacturers instead use a 2280 keyed layout.

Try to find an SSD from **Transcend** as they are known to be *reliable* among the Thinkpad community. Only problem is that they may be quite pricey or may even be unavailable to most individuals. Note that these drives do not support certain features that are taken for granted on SATA SSDs such as [DevSlp](https://www.transcend-info.com/Products/No-981) and may result in worse battery life. Generally speaking, the m.2 slot should be the last thing that should be upgraded on a T440p and skipped if possible to prioritise storage on the two SATA drive bays instead.

If you are one of the descibed individuals, you can *experiment* with **Chinese brand SSDs** like **Kingspec** from Aliexpress. There is no guarantee that these will be reliable despite positive reviews.

#### Additional 2.5 inch drive bay (with Ultranav)

Whilst having an optical disk drive is quite nice to have in case of the day you must watch your favourite TV series boxset that you just bought, or when your OS get corrupted and you have to reinstall your OS, it may still be left unused for the majority of time. Thankfully, the is a replacement option so that you can instead use this valuable space for additional storage!

This does **add an additional cost (<=$10)** on top of your storage drive, however the storage gains will be well worth this cost. Also, if you replaced the drive that was already included with your T440p, you could keep it and then re-purpose it purely as a storage drive.

### Post-upgrade recommendations

Now that you have upgraded your storage, there are a few things you should check.

#### BIOS

- Able to see the installed drive(s). If not, ensure that they are seated/installed correctly, otherwise there could be a bigger issue (e.g. faulty hardware)

#### Windows

- You have the Intel AHCI/storage drivers installed for optimal power usage and performance (highly recommended)
- Discarding of unused blocks is enabled (highly recommended)
- Disable search indexer if drive contains the OS[^3] (optional)

[^3]: Disabling search indexer can affect Windows search and various Microsoft apps

#### Linux

- Discarding of unused blocks is enabled by enabling `fstrim.timer`. If not, run:

        sudo systemctl enable fstrim.timer

- If you have TLP, ensure it is updated[^4] (recommended)
- Ensure a suitable I/O scheduler is selected (optional)
- You have a recent Linux kernel[^5] (optional)
- Configure swap, whether it is to use a swap file, or to reduce the swappiness[^6] (optional)
- Use a flash friendly file system[^7] (experimental)

[^4]: This is because in certain (old) configurations of TLP, storage power management may cause issues with reliability of data transfers

[^5]: Newer Linux kernels usually include many different bug fixes and improvements, some of them are related to the storage layer

[^6]: There have been many discussions on how swap affects the longevity of SSDs. In the end, it really depends on how you use your system. Choose what works best for **you**

[^7]: In general, you really shouldn't have to change your file system in order to optimise your SSD experience. One notable flash friendly file system is "F2FS" from Samsung

## Display

<div class="video-responsive">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/vOXz-tr41KM?start=419" frameborder="0" allowfullscreen></iframe>
</div>

If there is one upgrade that every T440p owner **should** do, it would be the display. The T440p comes with 3 display options out of the factory, with the HD (768p) option being the most commonly found.

It is unlikely that you would find a HD+ or FHD model on the internet, and this is because most organisations would choose for the lower resolution model as a cost-saving measure. This however is great for us enthusiasts as we can choose what model of display to swap with.

Now back to modding, swapping the display panel is quite easy to do. You can find a tutorial on how to do this on Youtube. Just remember to ensure that you remove the (external) battery before doing anything.

### What to buy?

The first two display panels were recommended by the Thinkpad community during the T440p's heyday but there are newer low power full HD panels that are now available that should be considered first.

|Make|Model|Type|Size|Coat|Color|Response|Contrast|View Angle|Brightness|Power|Comments|
|-|-|-|-|-|-|-|-|-|-|-|-
|LG|LP140WF3-SPD1 (LGD046D)|IPS|FHD|Matte|262K|700:1|25ms|80|300cd/m<sup>2</sup>|4.6W|
|Innolux|N140HCE-EN1 (CMN14D2)|IPS|FHD|Matte|262K|700:1|25ms|80|300cd/m<sup>2</sup>|4.0W|C2 or C4 model variants|
|Innolux|N140HCG-GQ2|IPS|FHD|Matte|16.7M|800:1|14ms|89|400cd/m<sup>2</sup>|3.1W|

Data obtained from [this reddit post](https://www.reddit.com/r/thinkpad/comments/86pw7g/best_t440p_ips_fhd_panel_march_2018/).

The LG and Innolux N140HCE panels spec-wise looks almost identical, with the only difference (apart from the make and model) being the power draw. If you had a choice between only those two panels, try looking for the Innolux panel. You can try **Aliexpress**, but be aware that some sellers may try to scam you by using fake item descriptions or even placing fake labelling on the panel itself!

The newer Innolux N140HCG-GQ2 panel is a better choice in pretty much every way as it is lower power, brighter, faster, and forwards compatible with newer ThinkPads which do not use tabs with screwholes so it should include adapter brackets to mount into the T440p's lid. The only real downside to choosing this panel is finding it and the higher cost but it should also give the T440p a minor battery life bump of 30-60 minutes over the former two panels. 

#### Alternatives

If the N140HCG-GQ2 is too difficult to find, there are three other low power FHD panels that could be considered being the AUO B140HAN05.7, BOE 	NE140FHM-61, or LG	LP140WF9_SPF1. They are slightly worse than the N140HCQ-GQ2 in response time and/or color accuracy but are still be better than the N140HCE-EN1 overall.

If the low power FHD panels are too difficult to obtain, then you can try the **B140HAN01.3 from AUO**, or the **NV140FHM-N62 from BOE**. The B140HAN01.3 suffers issues with a *yellowish* tint, meanwhile the BOE, while still being a great display, it uses a low PWM frequency for lowering display brightness (which may/may not be an issue for some). Alternatively, if you just want the absolute lowest cost FHD option, there is the BOE NV140FHM-N48 which comes in at between half to a third of the cost of the N140HCG-GQ2 (~$40-50).

## Touchpad

After Lenovo release the haswell-based *40 series of thinkpads, many followers and die-hard enthusiasts were outraged at Lenovo's decision to change the touchpad. The touchpad in the T440p features no physical buttons, which made the use of the trackpoint an inconvenience.

However all hope is not lost. It was found possible to retrofit the T450 touchpad, which has the touchpad with physical buttons, into the T440p. As of a result, this is also one of the most popular upgrades performed by users of this model.

### Where to purchase

There are many ways you can obtain a T450 touchpad. One common method is to buy the touchpad directly. You can try looking at _eBay_ or _Aliexpress_, but be aware of fake descriptions and scammers. Another method is to buy a T450 marked as for parts only, however be sure that the touchpad is functional (e.g. message the seller).

Also, you want to find a **synaptics** touchpad, rather than an alps one. If you do end up with an alps touchpad, fear not, it has been reported that it still works well.

### How to install touchpad

<div class="video-responsive">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/vOXz-tr41KM" frameborder="0" allowfullscreen></iframe>
</div>

### How to correctly install drivers

Follow the thinkpad forums guide: <https://forum.thinkpads.com/viewtopic.php?t=126860>

### Post-upgrade recommendations

#### General

Install rubber stoppers to prevent scratching the screen

- Whilst this upgrade is popular, it is not perfect
- There has been reported cases where the T450 touchpad has caused permanent damage to the display panel
- To prevent this, it is advised that you install rubber stoppers
- Ideally these should be larger than the ones currently present on your device (on each side of the webcam)

#### Windows

Prevent Windows from changing your drivers

- Visit the thinkpad forums guide: <https://forum.thinkpads.com/viewtopic.php?f=73&t=128283>
- Note: After a major Windows update, you may need to re-install your drivers

#### Linux

Use a desktop environment with touchpad gesture support

- Gnome 3 (Wayland)
- KDE
- Any desktop environment/linux distro combination with libinput support

## Keyboard

The T440p carries over the keyboard from the *30 series of thinkpads. Some people like it, others hate it. Also like previous thinkpads, the T440p has the signature trackpoint module.

In short, the trackpoint is a little red nub on the center of the keyboard that allows the user to control the mouse cursor, without having to leave their hands off the keyboard.

So now to the point that you've been waiting, what is there to upgrade? A quite popular upgrade is to swap over the standard T440p keyboard with an illuminated one. Why? Because it's cool? Who knows, and also why not?

### What to buy

This upgrade is probably more for the asethetics rather than for the functionality (who would use their laptop in a dark room anyway?) However that doesn't mean it's a complete waste of money.

When looking for a keyboard, look for one with the brand **Liteon**. This particular keyboard has been known to have a higher _build quality_ and _feel_ than the rest. There are other compatible keyboards you can use too, however build quality can not be guaranteed.

## Battery

If you have completed most of the upgrades above, you should by now have pretty powerful machine. However it is likely that you want your device to remain portable.

Buying second-hand usually means that the device has a bit of wear and tear, and the battery is no exception. One of the easiest ways you can increase your battery is to upgrade it to a larger one.

### Genuine batteries and capacities

There are multiple types of options you can choose from:

- Genuine
- Third-party

You can also choose between capacities:

- 6-cell
- 9-cell

You can probably find a ton of batteries online marked as genuine and possibly with a higher capacity. Be wary that some of the information you're seeing could be **false**.

The chances are that these batteries have been sitting in a storeroom for years. This would mean that the once awesome advertise capacity could be _less_ than in actual real-world figures.

You also may be lucky and get what is exactly advertised. If that is the case, please share with me your success story :)

All in all, the T440p is not the most efficient device out there. Compared to most modern laptops, idle battery drain is a lot higher, and also you shouldn't expect awesome battery life.

### What to buy

I am not going to debate whether you should go for a third-party or genuine battery. However, I have heard of positive feedback on the brand **Kingsener** from Aliexpress. Yes they are made in China, and it is a third-party battery.

The best advice I can give is to do proper research before purchasing your battery, as you are purchasing something that, by nature, can damage your hardware if not handled correcty.

### Post-upgrade recommendations

To calibrate or not to calibrate?

- It is generally advised that you do calibrate your battery before use
- This means that you charge your battery to 100%
- Then after fully-charged, deplete the battery down to at least 15%, before charging back full again
- It is best to use common-sense. If you look after the battery, it will last longer. Simple.

## Wifi

Wifi technology has improved greatly over the years thanks to some clever researchers. As of 2020, the newest and currently adopted standard is 802.11ax, or Wifi 6.

The Wifi adapter found on the T440p typically only supports Wireless-N networks, or Wifi-5. And whilst this is only decent at best, you may not be taking full advantage of your network speeds.

Now how to incorperate this new technology into the T440p. You purchase a new WiFi adapter only to find out that your laptop can no longer boot properly! This is because the T440p has a Wifi whitelist. Boo!

### How to get around this WiFi whitelist

#### Purchase a compatible Wifi adapter

You can purchase a compatible Wifi adapter to replace your current one. The problem, there aren't many options you can choose from. If you insist, the most modern adapter you can purchase is the **Intel Wireless AC 7260**. Availibility of this adapter cannot be guaranteed as it is an older adapter that is no longer supported by Intel.

#### Mod your BIOS to remove the whitelist

If you want to use any other Wifi adapter, you can mod your BIOS to remove the whitelist (and also to add more functionality to your BIOS). **There has been rumors that a previous BIOS may have a buggy whitelist, however this is not true.**

<div class="video-responsive">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/eIQYJWcc_-k" frameborder="0" allowfullscreen></iframe>
</div>

## Other tips

### Familiarise yourself with the trackpoint

This isn't necessarily a tip, but more like a PSA.

The trackpoint module is one of the Thinkpad's key defining features. USE IT, or at least learn how to. Why learn? The trackpoint exists to reduce the time it takes between typing and navigating the cursor. This obviously doesn't apply if you're not using a graphical environment (like a nerd, just kidding).

### Dust your device (if you haven't already)

This is not only to reduce thermals, but also to improve the logevity of the device. Assuming your device is second hand, there may still be build-up of dust inside the chassis. Use a can of compressed air or appropriate tool to do this.

### Use Linux (if you haven't thought of it)

Lenovo is known for building laptops with great support for Linux operating systems, and the T440p is no stranger to the Linux community. Why Linux? Linux is known to be less demanding on hardware than Windows. The downside is that the software that you need may not be available. For that, you can setup your system in a dual-boot or use a virtual machine (VM).

This is completely optional. Do your research before performing any operating system installation. There is also a risk that you may lose your data.

### You can tilt the screen parallel to the keyboard

The hinge of the T440p has been designed so that you can tilt the screen to be parallel to the keyboard (flat). Not sure why this may be useful, but it's there if you need it.

### Remove your battery when full-charged and when connected to AC power

Leaving your battery attached to your laptop can cause unnecessary wear. To prevent this, you can detach your battery and run solely on AC power. There isn't a downside of doing this (unless your power brick is too small).

### Use Thinkvantange for lenovo driver and software updates

One credit that Lenovo deserves is that they handle software updates quite well compared to other manufacturers. Their release software and driver updates regularly, however this does have a relation to how their Thinkpad product products are mainly geared towards enterprise environments.

This is entirely optional and there is no issue with installing updates yourself manually. However this may be preferred if you are doing this for someone who isn't very tech savvy.

### Use hardware acceleration on supported browsers

Hardware acceleration helps relieves the load on your CPU by displacing the load on your GPU. Your GPU can handle these kind of tasks much more efficiently than your CPU, which can help reduce thermals and increase performance.

The T440p is more than capable of handling HD videos. However don't expect it to handle 4K smoothly. You want to use hardware decoding for that.

#### Windows

- Most browsers (e.g. Firefox, Chrome, Edge) will support hardware video decoding
- Ensure you have the latest drivers installed

#### Linux

- Ensure you have the necessary VA-API drivers installed on your system
- Ensure you have a recent version of Mesa drivers installed
- If on Firefox
    - Currently Firefox 76+ whilst on Wayland supports this with changes to some flags
    - See: <https://mastransky.wordpress.com/2020/06/03/firefox-on-fedora-finally-gets-va-api-on-wayland/>
- If on Chromium
    - You may need to switch to a patched version of chromium
    - Depends on your Linux distro

        |Distro|Package Name|Repository
        |-|-|-
        |Arch|chromium-vaapi|[AUR](https://aur.archlinux.org/packages/chromium-vaapi/)
        |Ubuntu|chromium-beta/chromium-dev|[PPA](https://launchpad.net/~saiarcot895/+archive/ubuntu/chromium-beta)
        |Fedora|chromium-brower-privacy|[rpmfusion](https://admin.rpmfusion.org/pkgdb/package/free/chromium-browser-privacy/)
        |Opensuse|chromium|Official

### Booting Windows from GRUB is breaking the loading screen display

This can happen in the following cases:

- You are using GRUB
- Boot mode set to BIOS

To fix this, to configure GRUB to display as terminal mode only.

    # Uncomment to disable graphical terminal (grub-pc only)

    GRUB_TERMINAL=console

### Disable OS specific optimisations in BIOS

In the last of the BIOS, there should be an option to enable/disable OS optimisations. **You want to keep this disabled as it can mess up a few things that you don't want.**

### Be aware of BIOS supervisor passwords

When purchasing second-hand, you may obtain your device that has been previously used in a corperate environment.

This also means that it is likely that a supervisor password, or BIOS lock has been enabled. You can't disable this unless you know the password.

Why is this an issue? You may want to change settings in the BIOS that may enable you to dual-boot or enable virtualisation for virtual machines. This isn't possible if you have a BIOS lock and you don't know the password.

If you try to enter your BIOS and see a lock icon with password entry, this means your BIOS has a supervisor password set. Try to contact the seller and see if they know the password. If not, you may be out of luck.

A CMOS battery reset trick will not remove this password.

### Disable computrace

Computrace is piece of software that helps original owners of the device to locate and lock the device in the event of a robbery.

It is more commonly used in corperate environments where companies may loan out the device to their employees.

However as a general consumer, you may not need this software running. And also, you may not want to worry about the possibility of someone tracking you.

To disable this, enter to your BIOS (press F1 on initial boot), and locate computrace settings.You want to select **Permanently disable**.

### Using a mini-DP to HDMI converter

You may have noticed that the T440p has a mini displayport instead of HDMI. This is a more common occurance in enterprise products (and also helps in keeping costs down).

To use your T440p with a HDMI display, you need to use an adapter. However do not cheap out on an adapter, otherwise you may find that audio doesn't work. You can chose a mini-DP to HDMI adapter or cable, either way, just don't cheap out.

### Tweaking the microphone

There are a set of microphones visible on each of the sides of the webcam. These microphones features support for echo cancellation and beamforming. However they may not be enabled by default. You may notice that these features may work best whilst in Windows.

#### Windows

- Ensure you have Realtek HD Audio drivers installed
- Open Realtek Control panel, and under the microphone tab, enable Beamforming (BF) and Acoustic Echo Cancallation (AEC)

#### Linux

- Linux noise-cancellation can be a hit or miss due to having to work with many different audio devices
- When doing video web conferencing, some degree of noise cancelling is already done
- Try following this guide: <https://wiki.archlinux.org/index.php/PulseAudio/Troubleshooting#Enable_Echo/Noise-Cancellation>

### Improving the webcam quality

Let's face it, the camera quality is ****. How to improve it though? The only way to ensure that camera quality is optimal is to have enough lighting. Still not good enough? I'd consider purchasing a different webcam or laptop :)

### Install Coreboot (Experimental)

Recently the T440p gained support for Coreboot. Why is this a good thing? This is a step forward into having a free and open-source BIOS firmware implementation.

Please note that if you do perform this installaton, you may find that some aspects of your device (e.g. audio) might not work as expected.

See: <https://0xcb.dev/lenovo-t440p-coreboot/>

### Convert your Thinkpad into a Hackintosh

If you like MacOS, but you also like the hardware of the T440p, you can install MacOS with a bit of manual work.

<div class="video-responsive">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/XcOFR3y1m2s" frameborder="0" allowfullscreen></iframe>
</div>

## Performance
This section will present the performance of the T440p in some benchmarks that are representative of real world use cass compared to other processors.

### Web Browsing 
These web browsing benchmarks are relatively light loads that depend on how quick the CPU can switch between idle to full Turboboost clockspeeds. The T440p generally falls behind by a considerable margin because the old Haswell architecture simply has a much higher latency for switching clockspeeds compared to more modern [AMD](https://www.anandtech.com/show/14605/the-and-ryzen-3700x-3900x-review-raising-the-bar/4) and [Intel architectures](https://www.anandtech.com/show/9751/examining-intel-skylake-speed-shift-more-responsive-processors). There are no tweaks that can improve performance in these benchmarks for Haswell due to this being a fundamental architectural limitation.

**Speedometer 2.0**

|CPU|Architecture|Score (Runs/Minute)|Notes|
|-|-|-|-|
|i7 4900MQ|Haswell|56|Overclocked to 4Ghz in T440p, 60W PL1, from @fffffffffs|
|i7 1065G7|Ice Lake|78|[From AnandTech Review](https://www.anandtech.com/show/16084/intel-tiger-lake-review-deep-dive-core-11th-gen/10)|
|R7 4800U|Renoir|94|[From AnandTech Review](https://www.anandtech.com/show/16084/intel-tiger-lake-review-deep-dive-core-11th-gen/10)|
|i7 1185G7|Tiger Lake|113|15W, [from AnandTech Review](https://www.anandtech.com/show/16084/intel-tiger-lake-review-deep-dive-core-11th-gen/10)|

## Footnotes
