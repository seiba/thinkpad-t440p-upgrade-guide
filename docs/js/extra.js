$(document).ready(function() {
    // Fix table formatting: https://github.com/mkdocs/mkdocs/issues/2028
    $("table").each(function() {
        $(this).addClass("docutils");
    });
});
